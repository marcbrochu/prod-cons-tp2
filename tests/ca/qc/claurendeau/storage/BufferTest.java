package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {
	
	Element element;
	Buffer buffer;
	
	int capacity;
	
	@Before
	public void setUp() {
		capacity = 10;
		element = new Element();
		element.setData(1);
		buffer = new Buffer(capacity);
	}
	
	@After
	public void tearDown(){
		capacity = 0;
		element = null;
		buffer = null;
	}

    @Test
    public void testBufferCapacity() {
    	assertTrue(buffer.capacity() == capacity);
    }
    
    @Test
    public void testGetCurrentLoad() throws BufferFullException {
    	buffer.addElement(element);
    	assertTrue(buffer.getCurrentLoad() == 1);
    }
    
    @Test
    public void testIsEmpty() throws BufferFullException{
    	assertTrue(buffer.isEmpty());
    	buffer.addElement(element);
    	assertFalse(buffer.isEmpty());
    	
    }
    
    @Test
    public void testIsFull() throws BufferFullException{
    	assertFalse(buffer.isFull());
    	for (int i = 0; i < 10; i++) {
        	buffer.addElement(element);
		}
    	assertTrue(buffer.isFull());
    }
    
    @Test
    public void testRemoveElement() throws BufferFullException, BufferEmptyException{
    	buffer.addElement(element);
    	Element elementRemoved = buffer.removeElement();
    	assertTrue(elementRemoved.equals(element));
    }
    
    @Test
    public void testToString() throws BufferFullException {
    	buffer.addElement(element);
    	assertTrue(buffer.toString().equals("[1]"));
    }
    
    @Test(expected = BufferFullException.class)
    public void testBufferFullException() throws BufferFullException{
    	for (int i = 0; i < 11; i++) {
        	buffer.addElement(element);
		}
    }
    
    @Test(expected = BufferEmptyException.class)
    public void testBufferEmptyException() throws BufferEmptyException {
    	buffer.removeElement();
    } 

}


