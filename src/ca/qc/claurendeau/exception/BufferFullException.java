package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
	
    public BufferFullException(String message) {
        super(message);
    }
}
