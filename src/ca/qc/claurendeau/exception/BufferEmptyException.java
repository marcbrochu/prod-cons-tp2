package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {

	public BufferEmptyException(String message) {
        super(message);
	}
}
