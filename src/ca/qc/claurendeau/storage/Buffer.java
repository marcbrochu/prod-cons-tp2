package ca.qc.claurendeau.storage;

import java.util.Arrays;
import java.util.LinkedList;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
	private int capacity;
	private LinkedList<Element> linkedList;

    public Buffer(int capacity)
    {
    	this.capacity = capacity;
    	this.linkedList = new LinkedList<Element>();
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
        return Arrays.toString(linkedList.toArray());
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        return linkedList.size();
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        return linkedList.isEmpty();
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        return linkedList.size() >= capacity;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
    	if (isFull()) {
			throw new BufferFullException("Buffer is full");
		}
    	linkedList.add(element);
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException
    {
    	if (isEmpty()) {
			throw new BufferEmptyException("Buffer is empty");
		}
        return linkedList.remove();
    }
}
